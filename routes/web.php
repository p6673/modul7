<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/1/mahasiswa', function () {
    $nama = "Tya Kirana Putri";
    $nilai = [80, 64, 30, 76, 95];
    return view('1.mahasiswa', compact('nama', 'nilai')); 
});

Route::get('/2/mahasiswa', function () {
    return view('2.mahasiswa')->with('mahasiswa', ["Risa Lestari", "Rudi Hermawan", "Bambang Kusumo", "Lisa Permata"]);
});

Route::get('/2/dosen', function () {
    return view('2.dosen')->with('dosen', ["Maya Fitrianti, M.M.", "Prof. Silvia Nst, M.Farm.", "Dr. Umar Agustinus", "Dr. Syahrial, M.Kom."]);
});

// Route::get('/2/gallery', function () {
//     return view('2.gallery');
// });
Route::get('/3/universitas/fmipa/matematika/gallery', function () {
    return view('2.gallery');
})->name('gambar');

Route::get('/3/admin', function () {
    return view('3.admin');
});

Route::get('/3/informasi/{fakultas}/{jurusan}', function ($fakultas, $jurusan) {
    return view('3.informasi')->with('data', [$fakultas, $jurusan]);
})->name('info');
