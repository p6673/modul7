<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link @yield('menuMahasiswa')" href="/2/mahasiswa">Data Mahasiswa</a>
            </li>
            <li class="nav-item"><a class="nav-link @yield('menuDosen')" href="/2/dosen">Data Dosen</a></li>
            <li class="nav-item"><a class="nav-link @yield('menuGallery')"
                    href="{{ route('gambar') }}">Gallery</a></li>
            <li class="nav-item"><a class="nav-link @yield('menuInformasi')"
                    href="{{ route('info', ['fakultas' => 'FMIPA', 'jurusan' => 'Matematika']) }}">Info</a></li>
        </ul>
    </nav>
    @yield('content')

    <footer class="bg-dark py-4 text-white mt-4">
        <div class="container">
            Sistem Informasi Mahasiswa | Copyright &copy {{ date('Y') }} Duniailkom
        </div>
    </footer>
</body>

</html>
