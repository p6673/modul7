<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body>
    <div class="container text-center mt-3 p-4 bg-white">
        <h1>Halaman Admin</h1>
        <div class="row">
            <div class="col-12">
                @component('components.alert', ['class' => 'warning', 'judul' => 'Peringatan'])
                    100 data mahasiswa perlu diperbaiki
                @endcomponent

                @component('components.alert', ['class' => 'danger', 'judul' => 'Awas'])
                    Hari ini deadline laporan perjalanan dinas!
                @endcomponent

                @component('components.alert', ['class' => 'success', 'judul' => 'Kabar Baik'])
                    Bulan depan cuti panjang...
                @endcomponent
            </div>
        </div>
    </div>
</body>

</html>
